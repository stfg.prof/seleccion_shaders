# Pequeña seleccion de shaders 

Los mismos estan incrustados en paginas individuales corriendo en un entorno de [three.js](https://threejs.org/).

Para poder visualizarlos abrir el archivo index.htlm desde el navegador.

( en caso de complicaciones correr un servidor local recomiendo por facilidad de uso la extencion [liveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) para visula studio code )

--------
## Gemas
![fractfract](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/fractfract/fractfract.jpg)
--------
## S/T
![fullcolor](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/fullcolor/fullcolor.jpg)
--------
## Impercepciones (serie trituradores)
![impercepcion](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/impercepcion/impercepcion.jpg)
--------
## Nucleos
![nucleos](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/nucleos/nucleos.jpg)
--------
## luces
![luces](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/luces/luces.jpg)
--------
## ondas
![ondas](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/ondas/ondas.jpg)
--------
## molnar
![molnar](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/molnar/molnar.jpg)
--------
## circulos
![circulos](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/circulos/circulos.jpg)
--------
## aracne
###  shader realizados en el curso de sol Sarratea computando visiones del caos, derviados desde los [el fractal kotch](https://lashaderwiki.solsarratea.world/cursos/tematicos/computando-visiones-del-caos-en-glsl-por-cceba-media-lab/descripcion-sobre-las-clases/clase-1)
![aracne](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/aracne/aracne.jpg)
--------
## aracne 2
###  shader realizados en el curso de sol Sarratea computando visiones del caos, derviados desde los [el fractal kotch](https://lashaderwiki.solsarratea.world/cursos/tematicos/computando-visiones-del-caos-en-glsl-por-cceba-media-lab/descripcion-sobre-las-clases/clase-1)
![aracne-2](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/aracne-2/aracne-2.jpg)
--------
## pulso
![pulso](https://gitlab.com/stfg.prof/seleccion_shaders/-/raw/main/pulso/pulso.jpg)